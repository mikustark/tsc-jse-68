package ru.tsc.karbainova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.karbainova.tm.model.Project;

public interface IProjectRepository extends JpaRepository<Project, String> {
}
