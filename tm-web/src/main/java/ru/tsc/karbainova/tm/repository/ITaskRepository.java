package ru.tsc.karbainova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.karbainova.tm.model.Task;

public interface ITaskRepository extends JpaRepository<Task, String> {
}
