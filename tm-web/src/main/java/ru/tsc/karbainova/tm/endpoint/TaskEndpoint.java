package ru.tsc.karbainova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tsc.karbainova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/tasks")
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(taskService.findAll());
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") @WebParam(name = "id") final String id) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Task create(@RequestBody @WebParam(name = "task") final Task task) {
        taskService.save(task);
        return task;
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public Task save(@RequestBody @WebParam(name = "task") final Task task) {
        taskService.save(task);
        return task;
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") @WebParam(name = "id") final String id) {
        taskService.removeById(id);
    }
}
