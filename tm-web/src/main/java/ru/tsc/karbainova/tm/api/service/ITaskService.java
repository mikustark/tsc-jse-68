package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {
    List<Task> findAll();

    void addAll(Collection<Task> collection);

    Task save(Task entity);

    void create();

    Task findById(String id);

    void clear();

    void removeById(String id);

    void remove(Task entity);

}
