package ru.tsc.karbainova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.enumerated.Status;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.repository.ProjectRepository;
import ru.tsc.karbainova.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/task/create")
    public String create() {
        taskService.create();
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.removeById(id);
        return "redirect:/tasks";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        return new ModelAndView(
                "task-create",
                "task", taskService.findById(id)
        );
    }

    @PostMapping("/task/edit")
    public String edit(@ModelAttribute("task") Task task) {
        taskService.save(task);
        return "redirect:/tasks";
    }

    @ModelAttribute("viewName")
    public String getViewName() {
        return "Task edit";
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectService.findAll();
    }
}
