package ru.tsc.karbainova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.repository.ProjectRepository;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", projectService.findAll());
    }

    @ModelAttribute("viewName")
    public String getViewName() {
        return "Projects";
    }
}
