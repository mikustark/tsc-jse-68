package ru.tsc.karbainova.tm;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.karbainova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.karbainova.tm.client.ProjectFeignClient;
import ru.tsc.karbainova.tm.marker.RestCategory;
import ru.tsc.karbainova.tm.model.Project;

public class ProjectTest {

    final Project project1 = new Project("test project 1");

    final Project project2 = new Project("test project 2");

    final IProjectEndpoint client = ProjectFeignClient.client();

    @Before
    public void before() {
        client.create(project1);
    }

    @After
    @SneakyThrows
    public void after() {
    }

    @Test
    @Category(RestCategory.class)
    public void find() {
        client.find(project1.getId());
        Assert.assertEquals(project1.getName(), client.find(project1.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void create() {
        Assert.assertNotNull(client.create(project2));
    }

    @Test
    @Category(RestCategory.class)
    public void update() {
        final Project updatedProject = client.find(project1.getId());
        updatedProject.setName("updated");
        client.save(updatedProject);
        Assert.assertEquals("updated", client.find(project1.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void delete() {
        client.delete(project1.getId());
        Assert.assertNull(client.find(project1.getId()));
    }

    @Test
    @Category(RestCategory.class)
    public void findAll() {
        Assert.assertEquals(1, client.findAll().size());
        client.create(project2);
        Assert.assertEquals(2, client.findAll().size());
    }
}
