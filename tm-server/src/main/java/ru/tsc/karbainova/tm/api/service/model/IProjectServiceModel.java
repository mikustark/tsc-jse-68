package ru.tsc.karbainova.tm.api.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectServiceModel {
    @SneakyThrows
    List<Project> findAll();

    @SneakyThrows
    void clear();

    @SneakyThrows
    void addAll(Collection<Project> collection);

    @SneakyThrows
    Project add(Project project);

    @SneakyThrows
    void create(@NonNull String userId, @NonNull String name);

    @SneakyThrows
    void remove(@NonNull String userId, @NonNull Project project);

    @SneakyThrows
    Project updateById(@NonNull String userId, @NonNull String id,
                       @NonNull String name, @Nullable String description);

}
