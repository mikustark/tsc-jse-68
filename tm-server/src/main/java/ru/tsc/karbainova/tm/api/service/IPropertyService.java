package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tsc.karbainova.tm.api.ISaltSettings;
import ru.tsc.karbainova.tm.api.ISignatureSetting;

public interface IPropertyService extends ISaltSettings, ISignatureSetting {
    @NonNull String getJdbcUser();

    @NonNull String getJdbcPassword();

    @NonNull String getJdbcUrl();

    @NonNull String getJdbcDriver();

    @NonNull String getDialect();

    @NonNull String getAuto();

    @NonNull String getSqlShow();

    @NonNull String getApplicationVersion();

    @NonNull String getDeveloperName();

    @NonNull String getDeveloperEmail();

    @NonNull String getServerHost();

    @NonNull String getServerPort();

    @NotNull String getUseSecondLevelCache();

    @NotNull String getUseQueryCache();

    @NotNull String getUseMinimalPuts();

    @NotNull String getCacheRegionPrefix();

    @NotNull String getCacheProviderConfig();

    @NotNull String getCacheRegionFactory();
}
