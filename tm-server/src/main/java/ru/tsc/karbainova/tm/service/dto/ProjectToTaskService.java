package ru.tsc.karbainova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.repository.dto.ProjectDtoRepository;
import ru.tsc.karbainova.tm.api.repository.dto.TaskDtoRepository;
import ru.tsc.karbainova.tm.api.service.dto.IProjectToTaskService;
import ru.tsc.karbainova.tm.dto.ProjectDTO;
import ru.tsc.karbainova.tm.dto.TaskDTO;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;

import java.util.List;

import static org.reflections.util.Utils.isEmpty;

@Service
public class ProjectToTaskService implements IProjectToTaskService {

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @NotNull
    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @Override
    @Transactional
    @SneakyThrows
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        taskDtoRepository.bindTaskById(userId, projectId, taskId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        taskDtoRepository.unbindTaskById(userId, projectId, taskId);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDTO> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(userId)) throw new EmptyIdException();
        return taskDtoRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeProjectById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        taskDtoRepository.deleteByProjectId(id);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        ProjectDTO projectDTO = projectDtoRepository.findFirstByUserIdAndName(userId, name);
        taskDtoRepository.deleteByProjectId(projectDTO.getId());
    }
}
