package ru.tsc.karbainova.tm.exception;

public abstract class AbstractException extends RuntimeException {
    public AbstractException() {
        super();
    }

    public AbstractException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractException(String s) {
        super(s);
    }

    public AbstractException(Throwable cause) {
        super(cause);
    }
}
