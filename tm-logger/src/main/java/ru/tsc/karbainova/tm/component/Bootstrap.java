package ru.tsc.karbainova.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.api.service.IReceiverService;
import ru.tsc.karbainova.tm.listener.JmsLogListener;
import ru.tsc.karbainova.tm.service.JmsReceiverService;

import javax.jms.MessageListener;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    IReceiverService receiverService;

    @NotNull
    @Autowired
    MessageListener logListener;

    @SneakyThrows
    public void init() {
        receiverService.receive(logListener);
    }

}
