package ru.tsc.karbainova.tm.listener.serv;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractListener;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;

@Component
public class HelpDisplayListener extends AbstractSystemListener {
    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "All commands";
    }

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Override
    @EventListener(condition = "@helpListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener listener : listeners) {
            System.out.println(listener);
        }
    }
}
