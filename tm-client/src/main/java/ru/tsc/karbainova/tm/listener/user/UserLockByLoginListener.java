package ru.tsc.karbainova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;
import ru.tsc.karbainova.tm.listener.TerminalUtil;

@Component
public class UserLockByLoginListener extends AbstractSystemListener {
    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " Lock user";
    }

    @Override
    @EventListener(condition = "@userLockByLoginListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        adminEndpoint.lockUserByLoginUser(sessionService.getSession(), login);
    }

}
